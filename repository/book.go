package repository

import (
	"example-diya-don/entity"

	"gorm.io/gorm"
)

type Repository interface {
	FindAll() ([]entity.Book, error)
	FindById(id uint32) (entity.Book, error)
	Create(book entity.Book) (entity.Book, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) FindAll() ([]entity.Book, error) {
	var books []entity.Book
	err := r.db.Find(&books).Error
	return books, err
}

func (r *repository) FindById(id uint32) (entity.Book, error) {
	var book entity.Book
	err := r.db.First(&book, id).Error
	return book, err
}

func (r *repository) Create(book entity.Book) (entity.Book, error) {
	err := r.db.Save(&book).Error
	return book, err
}
