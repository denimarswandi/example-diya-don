package repository

import (
	"example-diya-don/entity"
	"log"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type UserRepository interface {
	InsertUser(user entity.User) entity.User
	UpdateUser(user entity.User) entity.User
	VerifyCredential(email string, password string) interface{}
	IsDuplicateEmail(email string) (tx *gorm.DB)
	FindByEmail(email string) entity.User
}

type userConnection struct {
	con *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &userConnection{con: db}
}

func (r *userConnection) InsertUser(user entity.User) entity.User {
	user.Password = hashAndSalt([]byte(user.Password))
	r.con.Save(&user)
	return user
}

func (r *userConnection) UpdateUser(user entity.User) entity.User {
	if user.Password != "" {
		user.Password = hashAndSalt([]byte(user.Password))
	} else {
		var tempUesr entity.User
		r.con.Find(&tempUesr, user.Id)
		user.Password = tempUesr.Password
	}
	r.con.Save(&user)
	return user
}

func (r *userConnection) VerifyCredential(email string, password string) interface{} {
	var user entity.User
	res := r.con.Where("email=?", email).Take(&user)
	if res.Error == nil {
		return user
	}
	return nil
}

func (r *userConnection) IsDuplicateEmail(email string) (tx *gorm.DB) {
	var user entity.User
	return r.con.Where("email=?", email).Take(&user)
}

func (r *userConnection) FindByEmail(email string) entity.User {
	var user entity.User
	r.con.Where("email=?", email).Take(&user)
	return user
}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
		panic("Failed to hash a password")
	}
	return string(hash)
}
