package main

import (
	"example-diya-don/config"
	"example-diya-don/router"

	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.SetupDatabaseConnection()
	//jwtData service.JWTService
)

func main() {
	defer config.ClosedbConnection(db)
	//gin.SetMode(gin.ReleaseMode)
	//jwtData = service.NewJWTService()
	r := router.RouterWrap(db)
	//ok := jwtData.GenerateToken("100")
	//fmt.Println(ok)
	r.Run(":9090")

}
