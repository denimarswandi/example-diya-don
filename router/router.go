package router

import (
	"example-diya-don/handler"
	"example-diya-don/middleware"
	"example-diya-don/repository"
	"example-diya-don/service"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

var (
	bookRepository repository.Repository
	bookService    service.Service
	bookHandler    handler.BookHandler
	userRepository repository.UserRepository
	//userService    service.UserService
	authService service.AuthService
	authHandler handler.AuthHandler
	jwtService  service.JWTService
)

func RouterWrap(db *gorm.DB) *gin.Engine {

	bookRepository = repository.NewRepository(db)
	bookService = service.NewService(bookRepository)
	bookHandler = handler.NewBookHandler(bookService)

	userRepository = repository.NewUserRepository(db)
	authService = service.NewAuthService(userRepository)
	jwtService = service.NewJWTService()
	authHandler = handler.NewAuthHandler(authService, jwtService)
	//userService = service.NewUserService(userRepository)

	r := gin.Default()
	v1 := r.Group("/v1", middleware.AuthorizeJWT(jwtService))
	v1.GET("/book", bookHandler.GetBooks)
	v1.POST("/book", bookHandler.PostBook)
	v2 := r.Group("/v2")
	v2.POST("/user/auth", authHandler.Register)
	v2.POST("/user/login", authHandler.Login)
	return r

}
