package service

import (
	"example-diya-don/dto"
	"example-diya-don/entity"
	"example-diya-don/repository"
)

type Service interface {
	FindAll() ([]entity.Book, error)
	Create(book dto.BookRequest) (dto.BookRequest, error)
	FindById(id uint32) (entity.Book, error)
}

type service struct {
	repository repository.Repository
}

func NewService(repository repository.Repository) *service {
	return &service{repository}
}

func (s *service) FindById(id uint32) (entity.Book, error) {
	book, err := s.repository.FindById(id)
	return book, err
}

func (s *service) FindAll() ([]entity.Book, error) {
	books, err := s.repository.FindAll()
	return books, err
}

func (s *service) Create(bookRequest dto.BookRequest) (dto.BookRequest, error) {
	price, _ := bookRequest.Price.Int64()
	book := entity.Book{
		Title:       bookRequest.Title,
		Description: bookRequest.Description,
		Price:       uint32(price),
		Discount:    bookRequest.Discount,
	}
	_, err := s.repository.Create(book)
	return bookRequest, err

}
