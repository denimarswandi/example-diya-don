package service

import (
	"example-diya-don/dto"
	"example-diya-don/entity"
	"example-diya-don/repository"
	"log"

	"github.com/mashingan/smapping"
)

type UserService interface {
	Update(user dto.UesrUpdateDto) entity.User
}

type userService struct {
	userRepository repository.UserRepository
}

func NewUserService(userRepo repository.UserRepository) UserService {
	return &userService{userRepository: userRepo}
}

func (s *userService) Update(user dto.UesrUpdateDto) entity.User {
	userToUpdate := entity.User{}
	err := smapping.FillStruct(&userToUpdate, smapping.MapFields(&user))
	if err != nil {
		log.Fatalf("Fieled map %v", err)
	}
	updateUser := s.userRepository.UpdateUser(userToUpdate)
	return updateUser
}
