package handler

import (
	"example-diya-don/dto"
	"example-diya-don/entity"
	"example-diya-don/service"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type AuthHandler interface {
	Login(c *gin.Context)
	Register(c *gin.Context)
}

type authHandler struct {
	authService service.AuthService
	jwtService  service.JWTService
}

func NewAuthHandler(authService service.AuthService, jwtService service.JWTService) AuthHandler {
	return &authHandler{
		authService,
		jwtService,
	}
}

func (a *authHandler) Login(c *gin.Context) {
	var loginDto dto.LoginDto
	errDto := c.ShouldBind(&loginDto)
	if errDto != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": fmt.Sprintln(errDto)})
		return
	}
	authResult := a.authService.VerifyCredential(loginDto.Email, loginDto.Password)
	if v, ok := authResult.(entity.User); ok {
		generateToken := a.jwtService.GenerateToken(strconv.FormatUint(uint64(v.Id), 10))
		v.Token = generateToken
		c.JSON(http.StatusOK, v)
	}

}

func (a *authHandler) Register(c *gin.Context) {
	var registerDto dto.RegisterDto
	errDto := c.ShouldBind(&registerDto)
	if errDto != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": errDto})
		return
	}
	//fmt.Println(a.authService.IsDuplicateEmail(registerDto.Email))
	if !(a.authService.IsDuplicateEmail(registerDto.Email)) {
		c.JSON(http.StatusConflict, gin.H{
			"error": "testnig saja dulu",
		})
		return
	} else {
		createdUser := a.authService.CreateUser(registerDto)
		token := a.jwtService.GenerateToken(strconv.FormatUint(uint64(createdUser.Id), 10))
		createdUser.Token = token
		c.JSON(http.StatusCreated, createdUser)
	}
}
