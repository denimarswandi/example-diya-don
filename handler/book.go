package handler

import (
	"example-diya-don/dto"
	"example-diya-don/entity"
	"example-diya-don/service"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

var validate *validator.Validate

type BookHandler interface {
	GetBooks(c *gin.Context)
	PostBook(c *gin.Context)
}

type bookHandler struct {
	bookService service.Service
}

func NewBookHandler(bookService service.Service) *bookHandler {
	return &bookHandler{bookService}

}

func (h *bookHandler) GetBooks(c *gin.Context) {
	books, err := h.bookService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": err,
		})
		return
	}

	var bookResponse []dto.BookResponse
	bookResponse = make([]dto.BookResponse, 0)
	for _, b := range books {
		bookResponse_ := convertToBookResponse(b)
		bookResponse = append(bookResponse, bookResponse_)
	}

	c.JSON(http.StatusOK, bookResponse)

}
func (h *bookHandler) PostBook(c *gin.Context) {
	fmt.Println("TEsting saja")
	validate = validator.New()
	var bookRequest dto.BookRequest
	c.ShouldBindJSON(&bookRequest)
	err := validate.Struct(bookRequest)
	if err != nil {
		errorMssage := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMssage_ := fmt.Sprintf("field: %s", e)
			errorMssage = append(errorMssage, errorMssage_)
		}
		c.JSON(http.StatusBadRequest, gin.H{
			"errors": errorMssage,
		})
		return
	}
	bookRequest_, err := h.bookService.Create(bookRequest)
	if err != nil {
		log.Print(err)
	}
	//fmt.rint(bookRequest_)
	c.JSON(http.StatusOK, bookRequest_)

}

func convertToBookResponse(b entity.Book) dto.BookResponse {
	return dto.BookResponse{
		Id:          b.Id,
		Title:       b.Title,
		Description: b.Description,
		Price:       b.Price,
		Discount:    b.Discount,
	}
}
