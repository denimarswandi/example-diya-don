package middleware

import (
	"example-diya-don/service"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func AuthorizeJWT(jwtService service.JWTService) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHandler := c.GetHeader("Authorization")
		if authHandler == "" {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"msg": "you must use token to access",
			})
			return
		}
		token, _ := jwtService.ValidateToken(strings.Split(authHandler, " ")[1])
		if token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			log.Println("Claim[user_id]:", claims["user_id"])
			log.Println("Claim[issuer]:", claims["iss"])
			log.Println("Claim[token]", claims["jti"])
			v := claims["exp"].(float64)
			tm := time.Unix(int64(v), 0)
			log.Println(tm)

		} else {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"msg": "uups",
			})

		}

	}
}
