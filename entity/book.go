package entity

import "time"

type Book struct {
	Id          uint32 `gorm:"type:int"`
	Title       string `gorm:"type:varchar(150)"`
	Description string `gorm:"type:text"`
	Price       uint32 `gorm:"type:int"`
	Discount    uint32 `gorm:"type:int"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
