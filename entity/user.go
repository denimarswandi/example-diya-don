package entity

type User struct {
	Id       uint32 `gorm:"primary_key:auto_increment" json:"id"`
	Name     string `gorm:"type:varchar(255)" json:"name"`
	Email    string `gorm:"unique;type:varchar(255)" json:"password"`
	Password string `gorm:"not null;type:varchar(255)" json:"-"`
	Token    string `gorm:"-" json:"token,omitempty"`
}
