package dto

import "encoding/json"

type BookRequest struct {
	Title       string      `json:"title" validate:"required"`
	Description string      `json:"description" validate:"required"`
	Price       json.Number `json:"price" validate:"required"`
	Discount    uint32      `json:"discount"`
}

type BookResponse struct {
	Id          uint32 `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Price       uint32 `json:"price"`
	Discount    uint32 `json:"discount"`
}
