package dto

type UesrUpdateDto struct {
	Id       uint32 `json:"id"`
	Name     string `json:"name" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Password string `json:"password"`
}
